const service = require('../service');
require('./styles.scss');

const editUrlInput = document.querySelector('#edit_url_popup');
const generateLinkButton = document.querySelector('#generateButton');
let currentTab = null;

service.getCurrentTab().then((tab) => {
    currentTab = tab;
    const tabUrl = _.get(tab, 'url', '');
    editUrlInput.value = tabUrl;
});

const handleClick = async () => {
    const url = editUrlInput.value;
    const title = _.get(currentTab, 'title', null);
    const tabId = _.get(currentTab, 'id', null);
    await service.makeShortAndCopy({title, tabId, url, refreshShortLink: true});
};

generateLinkButton.addEventListener('click', handleClick);
