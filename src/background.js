const Browser = require("webextension-polyfill");
const _ = require('lodash');
const service = require('./service');

service.getOptions().then((options) => {
    if (options.isEditUrl) {
        Browser.action.setPopup({popup: '../../edit_url_popup/edit_url_popup.html'});
    } else {
        Browser.action.setPopup({popup: ''});
    }
});

const handleClickIcon = async (tab) => {
    const currentTab = await service.getCurrentTab();
    const url = _.get(currentTab, 'url', null);
    const title = _.get(currentTab, 'title', null);
    const tabId = _.get(currentTab, 'id', null);
    await service.makeShortAndCopy({title, url, tabId, refreshShortLink: false})
};

Browser.action.onClicked.addListener(handleClickIcon);
