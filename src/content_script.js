const Browser = require("webextension-polyfill");
const service = require('./service');

Browser.runtime.onMessage.addListener( (request) => {
    if (request.message === "copyText"){
        service.copyLinkToClipboard(request.textToCopy);
    }
});

