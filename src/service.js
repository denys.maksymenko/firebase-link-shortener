const Browser = require("webextension-polyfill");
const _ = require('lodash');
const CONST = require('./const');

const getOptions = async () => {
    const storage = await Browser.storage.local.get();
    return { 
        firebaseDomain: _.get(storage, 'firebaseDomain', ''), 
        firebaseKey: _.get(storage, 'firebaseKey', ''),
        isEditUrl: _.get(storage, 'isEditUrl', '')
     };
};

const saveShortLink = async ({title, shortLink}) => {
    await Browser.storage.local.set({[title]: shortLink});
};

const getShortLink = async ({title}) => {
    const storage = await Browser.storage.local.get();
    return _.get(storage, title, null);
};

const clearlocalStorage = async () => {
    await Browser.storage.local.clear()
}

const requestShortLink = async ({ firebaseKey, firebaseDomain, link }) => {
    const requestParams = `?key=${firebaseKey}`;
    const requestBody = {
        dynamicLinkInfo: {
            domainUriPrefix: firebaseDomain,
            link
        }
    }
    const response = await fetch( CONST.FIRE_BASE_API_URL + requestParams,
        {
            method: 'POST',
            body: JSON.stringify(requestBody),
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    );

    const data = await response.json();
    const shortLink = _.get(data, 'shortLink', null);
    return shortLink;
}

const showNotification = async ({title, message}) => {
    await Browser.notifications.create({
        type: "basic",
        iconUrl: '../../images/icon.png',
        title,
        message
    });
};

const copyLinkToClipboard = (link) => {
    const copyFrom = document.createElement("textarea");
    copyFrom.textContent = link;
    document.body.appendChild(copyFrom);
    copyFrom.select();
    document.execCommand('copy');
    document.body.removeChild(copyFrom);
};

const getCurrentTab = async () => {
    const tabs = await Browser.tabs.query({currentWindow: true, active: true});
    console.log('tabs', tabs);
    if (tabs.length) {
        return _.first(tabs);
    }
    return null;
};

const makeShortAndCopy = async ({title, tabId, url, refreshShortLink}) => {
    const { firebaseKey, firebaseDomain } = await getOptions();

    let shortLink = await getShortLink({title});
    if (!shortLink || refreshShortLink) {
        const newShortLink = await requestShortLink({ firebaseKey, firebaseDomain, link: url});
        if(!newShortLink) {
           await showNotification({title: CONST.ERROR_TITLE, message: CONST.ERROR_MESSAGE});
           return;
        }
        await saveShortLink({title, shortLink: newShortLink});
        shortLink = newShortLink;
    }
    await showNotification({title: CONST.NOTIFICATION_TITLE, message: CONST.NOTIFICATION_MESSAGE + shortLink});
    await Browser.tabs.sendMessage(tabId, {
        message: "copyText",
        textToCopy:  shortLink
    });
} 

module.exports = {
    getOptions,
    saveShortLink,
    getShortLink,
    requestShortLink,
    clearlocalStorage,
    showNotification,
    copyLinkToClipboard,
    getCurrentTab,
    makeShortAndCopy
};