const Browser = require("webextension-polyfill");
const _ = require('lodash');
const service = require('../service');
require('./styles.scss');

const saveButton = document.querySelector('#saveButton');
const firebaseKey = document.querySelector('#firebaseKey');
const firebaseDomain = document.querySelector('#firebaseDomain');
const isEditUrl = document.querySelector('#isEditUrl');

const handleClick = async () => {
    const firebaseKeyValue = firebaseKey.value;
    const firebaseDomainValue = firebaseDomain.value;
    const isEditUrlValue = isEditUrl.checked;
    await Browser.storage.local.set({ firebaseKey: firebaseKeyValue,firebaseDomain: firebaseDomainValue, isEditUrl: isEditUrlValue});

    if (isEditUrlValue) {
        await Browser.action.setPopup({popup: '../../edit_url_popup/edit_url_popup.html'});
    } else {
        await Browser.action.setPopup({popup: ''});
    }

    alert('Options saved');
}

saveButton.addEventListener('click', handleClick);

service.getOptions().then((options) => {
    firebaseKey.value = options.firebaseKey;
    firebaseDomain.value = options.firebaseDomain;
    isEditUrl.checked = options.isEditUrl;
});