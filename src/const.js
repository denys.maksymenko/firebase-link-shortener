module.exports = {
    FIRE_BASE_API_URL : 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks',
    NOTIFICATION_TITLE: 'Short link create',
    NOTIFICATION_MESSAGE: `A short link create or already exists, now copied to the clipboard: `,
    ERROR_TITLE: 'Sorry, some error occured',
    ERROR_MESSAGE: 'Failed to create short link'
};